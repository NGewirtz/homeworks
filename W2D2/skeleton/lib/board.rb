class Board
  attr_accessor :cups

  def initialize(name1, name2)
    @cups = Array.new(14) { [:stone, :stone, :stone, :stone] }
    @cups[6], @cups[13] = [], []
    @p1 = name1
    @p2 = name2
  end

  def place_stones
    # helper method to #initialize every non-store cup with four stones each
  end

  def valid_move?(start_pos)
    raise "Invalid starting cup" unless (1..14).include?(start_pos) #&& @cups[start_pos].length > 0
  end

  def make_move(start_pos, current_player_name)
    stones = @cups[start_pos]
    last_cup = start_pos + stones.length
    return :prompt if last_cup == 13
    last_cup = (last_cup % 14) - 1 if last_cup > 13
    @cups[start_pos] = []

    stones.each do |stone|
      start_pos += 1
      start_pos += 1 if start_pos == 13
      cup_to_drop_idx = start_pos % 14
      @cups[cup_to_drop_idx] << stone
    end
    render
    next_turn(last_cup)
  end

  def next_turn(ending_cup_idx)
    # helper method to determine what #make_move returns
    return :switch if @cups[ending_cup_idx].empty?
    ending_cup_idx
    # :prompt
  end

  def render
    print "      #{@cups[7..12].reverse.map { |cup| cup.count }}      \n"
    puts "#{@cups[13].count} -------------------------- #{@cups[6].count}"
    print "      #{@cups.take(6).map { |cup| cup.count }}      \n"
    puts ""
    puts ""
  end

  def one_side_empty?
    @cups[0..5].all?(&:empty?) || @cups[7..12].all?(&:empty?)
  end

  def winner
    return :draw if @cups[6] == @cups[13]
    @cups[6].length > @cups[13].length ? @p1 : @p2
  end
end
