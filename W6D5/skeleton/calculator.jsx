import React from 'react';

class Calculator extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      result: 0,
      num1: "",
      num2: ""
    };
    this.setNum1 = this.setNum1.bind(this);
  }

  setNum1(e) {
    const target = e.currentTarget;
    const num1 = parseInt(target.value);
    this.setState({num1});
  }

  setNum2(e) {
    const target = e.currentTarget;
    const num2 = parseInt(target.value);
    this.setState({num2});
  }

  add(e) {
    e.preventDefault();
    const result = this.state.num1 + this.state.num2;
    this.setState({result});
  }

  subtract(e) {
    e.preventDefault();
    const result = this.state.num1 - this.state.num2;
    this.setState({result});
  }

  multiply(e) {
    e.preventDefault();
    const result = this.state.num1 * this.state.num2;
    this.setState({result});
  }

  divide(e) {
    e.preventDefault();
    const result = this.state.num1 / this.state.num2;
    this.setState({result});
  }

  clear(e) {
    const num1 = "";
    const num2 = "";
    this.setState({num1, num2});
  }

  render() {
    const { result, num1, num2 } = this.state;
    return (
      <div>
        <h1>{result}</h1>
        <input value={num1} type ='text'
          onChange={this.setNum1.bind(this)}></input>
        <input value={num2} type ='text'
          onChange={this.setNum2.bind(this)}></input>
        <button onClick={this.add.bind(this)}>+</button>
        <button onClick={this.subtract.bind(this)}>-</button>
        <button onClick={this.divide.bind(this)}>/</button>
        <button onClick={this.multiply.bind(this)}>*</button>
        <br />
        <button onClick={this.clear.bind(this)}>Clear</button>
      </div>
    );
  }
}

export default Calculator;
