# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


a = User.create(email: '1@1.com', password: 'starwars')
b = User.create(email: '2@2.com', password: 'notstarwars')
c = User.create(email: '3@3.com', password: 'starwars')
