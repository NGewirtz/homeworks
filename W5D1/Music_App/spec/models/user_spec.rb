require 'rails_helper'

RSpec.describe User, type: :model do
  it { should validate_presence_of(:email) }
  it { should validate_presence_of(:password_digest) }
  it { should validate_length_of(:password).is_at_least(6) }

  let(:neil) { User.new(email: 'neil@neil.com', password: 'starwars') }
  describe "::find_by_credentials" do
    it "returns a user with proper password" do
      expect(User.find_by_credentials('neil@neil.com', 'starwars')).not_to eq(nil)
    end
    it "returns nil with improper password" do
      expect(User.find_by_credentials('neil@neil.com', 'notstarwars')).to eq(nil)
    end
  end

  describe "#is_password?(password)" do
    it "returns true with the proper password" do
      expect(neil.is_password?('starwars')).to be(true)
    end

    it "returns false with an incorrect password" do
      expect(neil.is_password?('notstarwars')).to be(false)
    end
  end

  describe "#reset_session_token!" do
    before(:each) do
      User.first.destroy
    end
    it "resets the session token" do
      original = neil.session_token
      neil.reset_session_token!
      expect(original).not_to eq(neil.session_token)
    end

    it "returns a session token" do
      expect(neil.reset_session_token!).to eq(neil.session_token)
    end
  end

end
