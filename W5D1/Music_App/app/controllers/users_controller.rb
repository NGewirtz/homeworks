class UsersController < ApplicationController

  def new
    @user = User.new
  end

  def show
    @user = User.find(params[:id])
  end

  def create
    @user = User.new(user_params)
    if @user.save
      login(@user)
      msg = UserMailer.activation_email(@user)
      msg.deliver_now
      redirect_to bands_url
    else
      flash.now[:errors] = @user.errors.full_messages
      render :new
    end
  end

  def active
    user = User.find_by(activation_token: params[:activation_token])
    if user
      user.activated = true
      flash[:errors] = ["Account activated. You may now log in to continue"]
      redirect_to new_session_url
    else
      flash[:errors] = ["Account activation failed. Please email support for assistance"]
      redirect_to new_session_url
    end
  end

  private

  def user_params
    params.require(:user).permit(:email, :password)
  end

end
