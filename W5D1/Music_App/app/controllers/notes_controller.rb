class NotesController < ApplicationController
  before_action :ensure_logged_in

  def new
    @note = Note.new
  end

  def create
    @note = Note.new(note_params)
    @note.user = current_user
    @note.track = Track.find(params[:track_id])
    if @note.save
      redirect_to track_url(@note.track)
    else
      flash.now[:errors] = @note.errors.full_messages
      render :new
    end
  end

  def destroy
    @note = Note.find(params[:track_id])
    if current_user == @note.user
      @note.destroy
      redirect_to track_url(@note.track)
    else
      flash[:errors] = { errors: @note.errors.full_messages, code: 403 }
      redirect_to track_url(@note.track)
    end
  end

  private

  def note_params
    params.require(:note).permit(:body)
  end

end
