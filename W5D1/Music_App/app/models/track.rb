# == Schema Information
#
# Table name: tracks
#
#  id         :integer          not null, primary key
#  album_id   :integer          not null
#  title      :string           not null
#  ord        :integer          not null
#  bonus      :boolean          default(FALSE), not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  lyrics     :text
#

class Track < ApplicationRecord
  validates :album, :title, :ord, presence: true
  validates_inclusion_of :bonus, :in => [true, false]

  belongs_to :album

  has_one :band,
    through: :album,
    source: :band

  has_many :notes
end
