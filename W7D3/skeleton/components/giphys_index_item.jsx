import React from 'react';

const GiphysIndexItem = (props) => {
  return(
    <div>
      <h4>{props.giphy.slug}</h4>
      <img src={props.giphy.images.fixed_height.url} />
    </div>
  );
};


export default GiphysIndexItem;
