import React from 'react';

import GiphysIndexItem from './giphys_index_item';

const GiphysIndex = (props) => {
  const giphys = props.giphys.map((giphy, idx) => {
    return <GiphysIndexItem giphy = {giphy} key = {idx}/>;
  });
  return (
    <ul>
      {giphys}
    </ul>
  );
};

export default GiphysIndex;
