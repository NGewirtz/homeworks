import React from 'react';

import GiphysIndex from './giphys_index';

class GiphySearch extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      search: ''
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(e) {
    e.preventDefault();
    this.props.fetchSearchGiphys(e.currentTarget.value);
    this.state.search = '';
  }

  handleChange(e) {
    const search = e.currentTarget.value;
    this.setState({search});
  }

  render() {
    console.log(this.props.giphys);
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <input onChange={this.handleChange} value = {this.state.search}></input>
          <button>Search</button>
        </form>

        <GiphysIndex giphys = {this.props.giphys}/>
      </div>
    );
  }


}

export default GiphySearch;
