document.addEventListener("DOMContentLoaded", () => {
  // toggling restaurants

  const toggleLi = (e) => {
    const li = e.target;
    if (li.className === "visited") {
      li.className = "";
    } else {
      li.className = "visited";
    }
  };

  document.querySelectorAll("#restaurants li").forEach((li) => {
    li.addEventListener("click", toggleLi);
  });



  // adding SF places as list items
  let button = document.getElementsByClassName('favorite-submit')[0];
  let input = document.getElementsByClassName('favorite-input')[0];
  let ul = document.getElementById('sf-places');
  button.addEventListener('click', (e) => {
    e.preventDefault();
    let li = document.createElement('li');
    li.innerHTML = input.value;
    ul.append(li);
    input.value = '';
  });

  // --- your code here!



  // adding new photos

  // --- your code here!
  let photoButton = document.getElementsByClassName('photo-show-button')[0];
  photoButton.addEventListener('click', (e) => {
    let formDiv = document.getElementsByClassName('photo-form-container')[0];
    formDiv.classList.toggle('hidden');
  });

  let newPhotoButton = document.getElementsByClassName('photo-url-submit')[0];
  newPhotoButton.addEventListener('click', (e) => {
    e.preventDefault();
    let li = document.createElement('li');
    let img = document.createElement('img');
    let photoUrl = document.getElementsByClassName('photo-url-input')[0];
    let pupUl = document.getElementsByClassName('dog-photos')[0]
    img.src = photoUrl.value;
    li.append(img);
    pupUl.append(li);
  });



});
