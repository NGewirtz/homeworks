fish = ['fish', 'fiiish', 'fiiiiish', 'fiiiish', 'fffish', 'ffiiiiisshh', 'fsh', 'fiiiissshhhhhh']

def n2(arr)
  biggest = ""
  arr.each_with_index do |el, idx|
    arr.each do |fish|
      if arr[idx].length > fish.length
        biggest = arr[idx]
      end
    end
  end
  biggest
end


def nlogn_sort(arr)
  return arr if arr.length <= 1
  mid = arr.length / 2
  s_left = nlogn_sort(arr.take(mid))
  s_right = nlogn_sort(arr.drop(mid))
  nlogn_merge(s_left, s_right)
end

def nlogn_merge(left, right)
  arr = []
  until left.empty? || right.empty?
    case left.first.length <=> right.first.length
    when -1
      arr << left.shift
    when 0
      arr << left.shift
    when 1
      arr << right.shift
    end
  end
  arr.concat(left)
  arr.concat(right)
  arr
end

def n(arr)
  biggest = ''
  arr.each do |fish|
    biggest = fish if fish.length > biggest.length
  end
  biggest
end

tiles_array = ["up", "right-up", "right", "right-down", "down", "left-down", "left",  "left-up" ]

def slow_dance(target, arr)
  arr.each_with_index do |el, idx|
    return idx if el == target
  end
end

tiles_hash = {
  'up' => 0,
  'right-up' => 1,
  'right' => 2,
  'right_down' => 3,
  'down' => 4,
  'left-down' => 5,
  'left' => 6,
  'left-up' => 7
}

def fast_dance(target, hash)
  hash[target]
end


#p n2(fish)
p nlogn_sort(fish)
