
function madLib(verb, adj, noun) {
  console.log("We shall ${verb.toUppercase} the ${adj} ${noun}");
}

// madLib("make", "the", "best");

function isSubstring(search, sub) {
  return search.includes(sub)
}

// console.log(isSubstring("time to program", "time"))
// console.log(isSubstring("Jump for joy", "joys"))


function fizzBuzz(arr) {
  for(let i = 0; i < arr.length; i++) {
    if (arr[i] % 5 === 0 && arr[i] % 3 === 0) {
      continue
    } else if (arr[i] % 5 === 0 || arr[i] % 3 === 0 ) {
      console.log(arr[i])
    }
  }
}

// fizzBuzz([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16])


function isPrime(num) {
  for(let i = 2; i < num - 1; i++) {
    if (num % i === 0) {
      return false;
    }
  }
  return true;
}

// console.log(isPrime(2))
// console.log(isPrime(10))
// console.log(isPrime(15485863))
// console.log(isPrime(3548563))

function sumOfNPrimes(n) {
  if (n === 0) {
    return 0
  }
  let arr = []
  let i = 2
  while (arr.length < n) {
    if (isPrime(i)) {
      arr.push(i)
    }
    i++
  }
  return arr.reduce(function(a,b) {
    return a + b;
  });
}

// console.log(sumOfNPrimes(0))

function print(arr) {
  arr.forEach(function(el){
    console.log(el)
  })
}

function titleize(arr, func) {
  const titelized = arr.map(function(el){
    return "Mr " + el + " Jingleheimer Schmidt"
  });
  func(titelized)
}

// titleize(["Mary", "Brian", "Leo"], print);

function Elephant(name, height, arr) {
  this.name = name;
  this.height = height;
  this.tricks = arr;
}

Elephant.prototype.trumpet = function() {
  console.log(this.name + " the elephant goes 'phrRRRRRRRRRRR!!!!!!!")
}

Elephant.prototype.grow = function() {
  this.height += 12;
  console.log(this.height);
}

Elephant.prototype.addTrick = function(trick) {
  this.tricks.push(trick);
  console.log(trick);
}

Elephant.paradeHelper = function(elephant) {
  console.log(elephant.name + " is trotting by");
}

let ele = new Elephant('ele', 60, ['runs', 'jumps'])

ele.trumpet();
ele.grow();
ele.addTrick('plays poker');
console.log(ele.tricks);


let ellie = new Elephant("Ellie", 185, ["giving human friends a ride", "playing hide and seek"]);
let charlie = new Elephant("Charlie", 200, ["painting pictures", "spraying water for a slip and slide"]);
let kate = new Elephant("Kate", 234, ["writing letters", "stealing peanuts"]);
let micah = new Elephant("Micah", 143, ["trotting", "playing tic tac toe", "doing elephant ballet"]);

let herd = [ele, ellie, charlie, kate, micah];

Elephant.paradeHelper(micah)


herd.forEach(Elephant.paradeHelper)


function dinerBreakfast() {
  let order = "I'd like cheesy scrambled eggs"
  console.log(order)
  return function(food) {
    console.log(order + " and " + food + " please")
  }
}




let bfastOrder = dinerBreakfast();
bfastOrder("chocolate chip pancakes");
bfastOrder("grits");
