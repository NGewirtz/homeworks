class Stack
    def initialize
      # create ivar to store stack here!
      @stack = []
    end

    def add(el)
      # adds an element to the stack
      @stack.push(el)
    end

    def remove
      # removes one element from the stack
      @stack.pop
    end

    def show
      # return a copy of the stack
      @stack
    end
    #LIFO
  end


  class Queue
      def initialize
        @queue = []
      end

      def add(el)
        @queue.push(el)
      end

      def remove
        @queue.shift
      end

      def show
        @queue
      end
      #FIFO
    end

    class Map
      def initialize
        @map = []
      end

      def assign(key, val)
        if @map.any? { |sub_arr| sub_arr[0] == key }
          @map.each { |sub_arr| sub_arr[1] = val if sub_arr[0] == key }
        else
          @map << [key, val]
        end
        @map
      end

      def lookup(key)
        @map.select{ |sub_arr| sub_arr[0] == key }.first || nil
      end

      def remove(key)
        @map.delete(self.lookup(key))
      end
    end


    x = Map.new
    p x.assign(1,2)
    p x.assign(2,3)
    p x.assign(3,4)
    p x.assign(3,6)
    p x.lookup(3)
    p x.lookup(9)
    p x.remove(3)
    x
    p x.remove(9)
