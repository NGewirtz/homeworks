class Simon
  COLORS = %w(red blue green yellow)

  attr_accessor :sequence_length, :game_over, :seq

  def initialize
    @sequence_length = 1
    @game_over = false
    @seq = []
  end

  def play
    take_turn until game_over
    game_over_message
    reset_game
  end

  def take_turn
    show_sequence
    require_sequence
    round_success_message unless game_over
    self.sequence_length += 1
  end

  def show_sequence
    add_random_color
  end

  def require_sequence
    player_input = gets.chomp
    self.game_over = true unless player_input == self.seq
  end

  def add_random_color
    @seq << COLORS.sample
  end

  def round_success_message
    "Round Success!!"
  end

  def game_over_message
    "Game Over"
  end

  def reset_game
    self.sequence_length = 1
    self.game_over = false
    self.seq = []
  end
end
